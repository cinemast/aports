# Contributor: Curt Tilmes <Curt.Tilmes@nasa.gov>
# Maintainer: Curt Tilmes <Curt.Tilmes@nasa.gov>
pkgname=primesieve
pkgver=7.7
pkgrel=0
pkgdesc="Program and library for generating prime numbers"
url="http://primesieve.org"
arch="all"
license="BSD-2-Clause"
makedepends="cmake"
subpackages="$pkgname-dev $pkgname-doc"
source="primesieve-$pkgver.tar.gz::https://github.com/kimwalisch/primesieve/archive/v$pkgver.tar.gz"

build() {
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_TESTS=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
bbcc141a12afdf54386d2408e067b1c6af2dd9b5c9f44d71290269ddefc7c46ba6794064cde6728c8356724553cb6bc2e765bf18c24a18b6d5d1f472d2c6e6dd  primesieve-7.7.tar.gz
"
