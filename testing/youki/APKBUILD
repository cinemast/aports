# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=youki
pkgver=0.0.1
pkgrel=0
pkgdesc="A container runtime written in Rust"
url="https://github.com/containers/youki"
arch="aarch64 armhf armv7 ppc64le x86 x86_64"  # blocked by rust/cargo
license="Apache-2.0"
makedepends="
	cargo
	dbus-dev
	libgit2-dev
	libseccomp-dev
	zlib-dev
	"
subpackages="$pkgname-dbg"
source="https://github.com/containers/youki/archive/v$pkgver/$pkgname-$pkgver.tar.gz
	libcgroups-workaround-magic-constants.patch
	fix-rlimit-type.patch
	minimize-size.patch
	ungit.patch
	"
options="!check"  # FIXME: some tests fail

# Disable systemd_cgroups
_cargo_opts='--frozen --no-default-features --features=libcgroups/v1,libcgroups/v2'

prepare() {
	default_prepare

	# This file shouldn't be here! https://github.com/containers/youki/issues/617
	rm .cargo/config*

	cargo fetch --locked
}

build() {
	cargo build $_cargo_opts --release
}

check() {
	cargo test $_cargo_opts
}

package() {
	install -D -m755 target/release/youki -t "$pkgdir"/usr/bin/
}

sha512sums="
d5c976c922d47babb9cf7fcc324df34ac312a9cf56b5d7156a96595d30e36d65a56e2d15c7a0dfd3bc41d9ea4bce995e4af85f251a227060ff193e70c4b8ae51  youki-0.0.1.tar.gz
11e3ad8869b39283e9720454311f7393f3d7618e5530978423230bd16e03c7d0e3f390a0bd740a769af35cc7c6c9f433c5b38a4aa1059aea0f93fa0cf0ea11af  libcgroups-workaround-magic-constants.patch
4f193c21b7efc2f7322eb490fe4ec0a077a21013e1bfff1281f362310a09be62d11925c8d4d7c47197b371ad9635e53bb486a4e032dc0f56bdf6d195258fdb0c  fix-rlimit-type.patch
7587c055bf03371486d43b476d32d9d4a0008ce4ee7d8c3865f4ec82aa64134157aed914f41ffc80561444241f9d249fde878f4c77cdde0afd23ea8dd6484d24  minimize-size.patch
e6265e0cf567e1557491e2883b56bebfd7b23ced2fc65fc9c83e15beb5c0f53f90de10d5fe059ed933a878af2ee38fd00d9ce624622369e1101d32b71ac3b5a6  ungit.patch
"
