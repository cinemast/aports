# Contributor: George Hopkins <george-hopkins@null.net>
# Maintainer: George Hopkins <george-hopkins@null.net>
pkgname=imhex
pkgver=1.12.1
pkgrel=0
_nativefiledialog=322d1bc2a98c7b8236195d458643ac8e76391011
_xdgpp=f01f810714443d0f10c333d4d1d9c0383be41375

pkgdesc="Hex editor for reverse engineers and programmers"
url="https://github.com/WerWolv/ImHex"
arch="all !x86 !armhf !armv7 !mips" # See #11
license="GPL-2.0-or-later"
options="!check" # No testsuite
makedepends="
	capstone-dev
	cmake
	curl-dev
	file-dev
	fmt-dev
	freetype-dev
	glfw-dev
	glm-dev
	gtk+3.0-dev
	llvm-dev
	llvm-static
	mbedtls-dev
	nlohmann-json
	openssl-dev
	python3-dev
	yara-dev
	"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/WerWolv/ImHex/archive/v$pkgver.tar.gz
	nativefiledialog-$_nativefiledialog.tar.gz::https://github.com/btzy/nativefiledialog-extended/archive/$_nativefiledialog.tar.gz
	https://git.sr.ht/~danyspin97/xdgpp/blob/$_xdgpp/xdg.hpp
	fix-install-path.patch
	"
builddir="$srcdir"/ImHex-$pkgver

prepare() {
	default_prepare

	rmdir "$builddir"/external/nativefiledialog # overwrite
	mv "$srcdir"/nativefiledialog-extended-$_nativefiledialog "$builddir"/external/nativefiledialog
	mv "$srcdir"/xdg.hpp "$builddir"/external/xdgpp
}

build() {
	# project cmake doesn't add llvm lib path correctly,
	# this is the easiest fix
	export LDFLAGS="$LDFLAGS -L$(llvm-config --libdir)"
	cmake -B build \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_VERBOSE_MAKEFILE=ON \
		-DUSE_SYSTEM_CURL=ON \
		-DUSE_SYSTEM_NLOHMANN_JSON=ON \
		-DUSE_SYSTEM_FMT=ON \
		-DUSE_SYSTEM_LLVM=ON \
		-DUSE_SYSTEM_YARA=ON \
		-DUSE_SYSTEM_CAPSTONE=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	rm -r "$pkgdir"/usr/DEBIAN
}

sha512sums="
d0e1c993e37d61afc214a1a85310b3339ec783f5e0a0a527117709594fbd5224e850706e418baa4f4e01d6a40ac94798cef8e844ab2b5eb151d21d4cb836a784  imhex-1.12.1.tar.gz
049a19d39707476a609a6e22f70146df9e1533c5e93b349437d762673d6a1410777181fedff14ca6cf64e2c1e707c85f13d9101370aadbc79afd3795fac792a8  nativefiledialog-322d1bc2a98c7b8236195d458643ac8e76391011.tar.gz
0cb56ded30b4eb9d4fc3b4879d9bb5d5c084e67bfe9820264c1ae606f4964e18391cf327fd64b0ca42f89c5c469afa9622b003a1642c584e3da249caefd0373f  xdg.hpp
b10a1bf6c286f79eca39bfddfb5686280bfdc02967150399a3da3fc73470a888ca54ece36c4f2436058eb0466c61eddd82ee6145304bc9dacbbd2e1cd010356c  fix-install-path.patch
"
