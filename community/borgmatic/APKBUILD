# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=borgmatic
pkgver=1.5.22
pkgrel=0
pkgdesc="Simple, configuration-driven backup software for servers and workstations"
url="https://torsion.org/borgmatic/"
license="GPL-3.0-or-later"
arch="noarch !s390x" # tests fail on s390x
depends="
	borgbackup
	python3
	py3-setuptools
	py3-jsonschema
	py3-requests
	py3-ruamel.yaml
	py3-colorama
	"
checkdepends="
	py3-pytest
	py3-pytest-cov
	py3-flexmock
	"
source="$pkgname-$pkgver.tar.gz::https://projects.torsion.org/borgmatic-collective/borgmatic/archive/$pkgver.tar.gz"
builddir="$srcdir/borgmatic"

build() {
	python3 setup.py build
}

check() {
	# omit a simple test that requires borgmatic to be available in $PATH
	pytest -k "not test_borgmatic_version_matches_news_version"
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir" --skip-build
}

sha512sums="
8a96fd5f23332e3ad742717f9f2cfbb34dda459988c5aaca0db8dd8ccbbbccc0d9eec535e596b36358f174043ddacdb39378e1758882387290a9754f4cc04550  borgmatic-1.5.22.tar.gz
"
