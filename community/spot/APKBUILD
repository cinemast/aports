# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=spot
pkgver=0.3.0
pkgrel=1
pkgdesc="Native Spotify client for the GNOME desktop"
url="https://github.com/xou816/spot"
arch="aarch64 armhf armv7 ppc64le x86 x86_64"  # limited by rust/cargo
license="MIT"
makedepends="
	alsa-lib-dev
	bash
	cargo
	curl-dev
	glib-dev
	gtk4.0-dev
	libadwaita-dev
	libhandy1-dev
	meson
	nghttp2-dev
	openssl1.1-compat-dev
	pulseaudio-dev
	"
subpackages="$pkgname-lang"
source="https://github.com/xou816/spot/archive/$pkgver/spot-$pkgver.tar.gz
	pulseaudio-backend-only.patch
	"

prepare() {
	default_prepare

	# Optimize binary for size (20 MiB -> 6.1 MiB).
	cat >> Cargo.toml <<-EOF

		[profile.release]
		codegen-units = 1
		lto = true
		opt-level = "z"
		panic = "abort"
	EOF
}

build() {
	# NOTE: buildtype must be release!
	abuild-meson \
		-Doffline=false \
		--buildtype=release \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	# Meson eats stdout/stderr, so run cargo directly.
	cargo test --locked
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
8d90379db5e8317a2816c0f0057cb0a00dd30d41e046e5b1e011e353bb1f1318dc7e6d7faad9fca5204655c8b96d25a15bc46427c370b19f557a36fa79b20467  spot-0.3.0.tar.gz
f790d51e5ce61f47b27c516931405a26270682066b00f807cf0f1427eabfff58b37a3c3627db1cfa53de13488f22429c410844205772020fbc27304f228fc4a1  pulseaudio-backend-only.patch
"
